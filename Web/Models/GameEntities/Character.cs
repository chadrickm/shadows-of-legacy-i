﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models.GameEntities
{
    public class Character
    {
        public int CharacterId { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required, MaxLength(20)]
        public string FirstName { get; set; }
        
        [MaxLength(20)]
        public string LastName { get; set; }

        public string Gender { get; set; }

        public string Looks { get; set; }

        public string BirthSetting { get; set; }
        public virtual List<BackgroundSetting> BackgroundSettings { get; set; }
        public string StartingSetting { get; set; }
        public int StartingAge { get; set; }

        public bool IsBackgroundFinished { get; set; }
    }
}