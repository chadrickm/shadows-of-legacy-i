﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Models.GameEntities
{
    public class BackgroundSetting
    {
        public int BackgroundSettingId { get; set; }

        [ForeignKey("Character")]
        public int CharacterId { get; set; }
        public Character Character { get; set; }

        public string SettingKey { get; set; }

        public int YearsInSetting { get; set; }

        public int OrderBy { get; set; }
    }
}