﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Models.GameEntities
{
    public class Domain
    {
        public int DomainId { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [ForeignKey("Ruler")]
        public int? RulerId { get; set; }
        public virtual Character Ruler { get; set; }
    }
}