﻿using System;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Security;
using System.Security.Principal;
using Breeze.WebApi;
using Web.Models.Todos;

// ReSharper disable InconsistentNaming
namespace Web.Models.GameEntities
{
    public class GameRepository : EFContextProvider<GameContext>
    {
        public GameRepository(IPrincipal user)
        {
            UserId = user.Identity.Name;
        }

        public string UserId { get; private set; }

        public DbQuery<Domain> Domains
        {
            get
            {
                return (DbQuery<Domain>)Context.Domains
                    .Where(t => t.UserId == UserId);
            }
        }

        public DbQuery<Character> Characters
        {
            get
            {
                return (DbQuery<Character>)Context.Characters;
                    //.Where(t => t.UserId == UserId);
            }
        }

        public DbQuery<BackgroundSetting> BackgroundSettings
        {
            get { return Context.BackgroundSettings; }
        }

        #region Save processing

        // Todo: delegate to helper classes when it gets more complicated

        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            var entity = entityInfo.Entity;
            if (entity is Character)
            {
                return BeforeSaveCharacter(entity as Character, entityInfo);
            }
            if (entity is BackgroundSetting)
            {
                return true;
            }
            if (entity is Domain)
            {
                return BeforeSaveDomain(entity as Domain, entityInfo);
            }
            if (entity is TodoList)
            {
                return BeforeSaveTodoList(entity as TodoList, entityInfo);
            }
            if (entity is TodoItem)
            {
                return BeforeSaveTodoItem(entity as TodoItem, entityInfo);
            }
            throw new InvalidOperationException("Cannot save entity of unknown type");
        }


        private bool BeforeSaveCharacter(Character character, EntityInfo info)
        {
            if (info.EntityState == EntityState.Added)
            {
                character.UserId = UserId;
                return true;
            }
            return UserId == character.UserId || throwCannotSaveEntityForThisUser();
        }

        private bool BeforeSaveDomain(Domain domain, EntityInfo info)
        {
            if (info.EntityState == EntityState.Added)
            {
                domain.UserId = UserId;
                return true;
            }
            return UserId == domain.UserId || throwCannotSaveEntityForThisUser();
        }

        private bool BeforeSaveTodoList(TodoList todoList, EntityInfo info)
        {
            if (info.EntityState == EntityState.Added)
            {
                todoList.UserId = UserId;
                return true;
            }
            return UserId == todoList.UserId || throwCannotSaveEntityForThisUser();
        }

        private bool BeforeSaveTodoItem(TodoItem todoItem, EntityInfo info)
        {
            var todoList = ValidationContext.TodoLists.Find(todoItem.TodoListId);
            return (null == todoList)
                       ? throwCannotFindParentTodoList()
                       : UserId == todoList.UserId || throwCannotSaveEntityForThisUser();
        }

        // "this.Context" is reserved for Breeze save only!
        // A second, lazily instantiated DbContext will be used
        // for db access during custom save validation. 
        // See this stackoverflow question and reply for an explanation:
        // http://stackoverflow.com/questions/14517945/using-this-context-inside-beforesaveentity
        private GameContext ValidationContext
        {
            get { return _validationContext ?? (_validationContext = new GameContext()); }
        }
        private GameContext _validationContext;

        private static bool throwCannotSaveEntityForThisUser()
        {
            throw new SecurityException("Unauthorized user");
        }

        private bool throwCannotFindParentTodoList()
        {
            throw new InvalidOperationException("Invalid TodoItem");
        }

        #endregion

    }
}