﻿using System.Data.Entity;
using Web.Models.GameEntities;
using Web.Models.Todos;

namespace Web.Models
{
    // You can add custom code to this file. Changes will not be overwritten.
    // 
    // If you want Entity Framework to drop and regenerate your database
    // automatically whenever you change your model schema, add the following
    // code to the Application_Start method in your Global.asax file.
    // Note: this will destroy and re-create your database with every model change.
    // 
    // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<Web.Models.TodoItemContext>());
    public class GameContext : DbContext
    {
        public GameContext()
            : base("name=DefaultConnection")
        {
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Domain> Domains { get; set; }
        public DbSet<BackgroundSetting> BackgroundSettings { get; set; }

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<TodoList> TodoLists { get; set; }
    }
}