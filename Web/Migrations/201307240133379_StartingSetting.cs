namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StartingSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "StartingSetting", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Characters", "StartingSetting");
        }
    }
}
