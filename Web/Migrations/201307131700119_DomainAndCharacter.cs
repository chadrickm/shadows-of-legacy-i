namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DomainAndCharacter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Characters",
                c => new
                    {
                        CharacterId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 20),
                        LastName = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.CharacterId);
            
            CreateTable(
                "dbo.Domains",
                c => new
                    {
                        DomainId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        RulerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DomainId)
                .ForeignKey("dbo.Characters", t => t.RulerId, cascadeDelete: true)
                .Index(t => t.RulerId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Domains", new[] { "RulerId" });
            DropForeignKey("dbo.Domains", "RulerId", "dbo.Characters");
            DropTable("dbo.Domains");
            DropTable("dbo.Characters");
        }
    }
}
