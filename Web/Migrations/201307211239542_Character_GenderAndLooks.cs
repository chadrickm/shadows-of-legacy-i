namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Character_GenderAndLooks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "Gender", c => c.String());
            AddColumn("dbo.Characters", "Looks", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Characters", "Looks");
            DropColumn("dbo.Characters", "Gender");
        }
    }
}
