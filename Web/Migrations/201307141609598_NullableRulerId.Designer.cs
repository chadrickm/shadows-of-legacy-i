// <auto-generated />
namespace Web.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class NullableRulerId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(NullableRulerId));
        
        string IMigrationMetadata.Id
        {
            get { return "201307141609598_NullableRulerId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
