namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class YearsInSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BackgroundSettings", "YearsInSetting", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BackgroundSettings", "YearsInSetting");
        }
    }
}
