namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Character_BackgroundSettings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BackgroundSettings",
                c => new
                    {
                        BackgroundSettingId = c.Int(nullable: false, identity: true),
                        CharacterId = c.Int(nullable: false),
                        SettingKey = c.String(),
                        OrderBy = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BackgroundSettingId)
                .ForeignKey("dbo.Characters", t => t.CharacterId, cascadeDelete: true)
                .Index(t => t.CharacterId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.BackgroundSettings", new[] { "CharacterId" });
            DropForeignKey("dbo.BackgroundSettings", "CharacterId", "dbo.Characters");
            DropTable("dbo.BackgroundSettings");
        }
    }
}
