namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NonRequiredLastName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Characters", "LastName", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Characters", "LastName", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
