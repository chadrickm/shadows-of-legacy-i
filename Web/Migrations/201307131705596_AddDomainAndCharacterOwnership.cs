namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDomainAndCharacterOwnership : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.Domains", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Domains", "UserId");
            DropColumn("dbo.Characters", "UserId");
        }
    }
}
