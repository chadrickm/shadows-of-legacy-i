namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NoAge : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Characters", "Age");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Characters", "Age", c => c.Int(nullable: false));
        }
    }
}
