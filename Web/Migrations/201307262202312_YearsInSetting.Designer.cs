// <auto-generated />
namespace Web.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class YearsInSetting : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(YearsInSetting));
        
        string IMigrationMetadata.Id
        {
            get { return "201307262202312_YearsInSetting"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
