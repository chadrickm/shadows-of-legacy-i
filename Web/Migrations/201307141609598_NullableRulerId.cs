namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableRulerId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Domains", "RulerId", "dbo.Characters");
            DropIndex("dbo.Domains", new[] { "RulerId" });
            AlterColumn("dbo.Domains", "RulerId", c => c.Int());
            AddForeignKey("dbo.Domains", "RulerId", "dbo.Characters", "CharacterId");
            CreateIndex("dbo.Domains", "RulerId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Domains", new[] { "RulerId" });
            DropForeignKey("dbo.Domains", "RulerId", "dbo.Characters");
            AlterColumn("dbo.Domains", "RulerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Domains", "RulerId");
            AddForeignKey("dbo.Domains", "RulerId", "dbo.Characters", "CharacterId", cascadeDelete: true);
        }
    }
}
