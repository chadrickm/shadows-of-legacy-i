namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Character_BirthSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "BirthSetting", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Characters", "BirthSetting");
        }
    }
}
