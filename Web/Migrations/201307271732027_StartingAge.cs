namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StartingAge : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Characters", "StartingAge", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Characters", "StartingAge");
        }
    }
}
