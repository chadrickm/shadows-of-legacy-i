﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Breeze.WebApi;
using Newtonsoft.Json.Linq;
using Web.Filters;
using Web.Models.GameEntities;

namespace Web.Controllers
{
    public class BackgroundSettingsController : ApiController
    {
        private readonly GameRepository _repository;

        public BackgroundSettingsController()
        {
            _repository = new GameRepository(User);
        }

        // GET ~/api/Todo/Metadata 
        [HttpGet]
        public string Metadata()
        {
            return _repository.Metadata();
        }

        // POST ~/api/Todo/SaveChanges
        [HttpPost]
        [ValidateHttpAntiForgeryToken]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            return _repository.SaveChanges(saveBundle);
        }

        // GET ~/api/Todo/TodoList
        [HttpGet]
        public IQueryable<BackgroundSetting> BackgroundSettings()
        {
            return _repository.BackgroundSettings;
            // We do the following on the client
            //.Include("Children")
            //.OrderByDescending(t => t.OrderBy);
        }
    }
}
