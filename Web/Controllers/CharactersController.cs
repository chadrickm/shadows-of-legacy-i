﻿using Web.Models.GameEntities;

namespace Web.Controllers
{
    using System.Linq;
    using System.Web.Http;
    using Breeze.WebApi;
    using Filters;
    using Newtonsoft.Json.Linq;

    [Authorize]
    [BreezeController]
    public class CharactersController : ApiController
    {
        private readonly GameRepository _repository;

        public CharactersController()
        {
            _repository = new GameRepository(User);
        }

        // GET ~/api/Todo/Metadata 
        [HttpGet]
        public string Metadata()
        {
            return _repository.Metadata();
        }

        // POST ~/api/Todo/SaveChanges
        [HttpPost]
        [ValidateHttpAntiForgeryToken]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            return _repository.SaveChanges(saveBundle);
        }

        // GET ~/api/Todo/TodoList
        [HttpGet]
        public IQueryable<Character> Characters()
        {
            return _repository.Characters;
            // We do the following on the client
            //.Include("Todos")
            //.OrderByDescending(t => t.TodoListId);
        }
    }
}