﻿'use strict'; 

game.factory('TypesService', function() {

    var typesService = {
        getYearsInSetting: getYearsInSetting,
    };

    return typesService;
    
    function getYearsInSetting(currentAge) {
        var availAges = [
            { key: 1, display: '1 Year' },
            { key: 5, display: '5 Years' },
            { key: 10, display: '10 Years' },
            { key: 15, display: '15 Years' },
            { key: 20, display: '20 Years' },
            { key: 25, display: '25 Years' },
            { key: 30, display: '30 Years' },
            { key: 35, display: '35 Years' },
            { key: 40, display: '40 Years' },
            { key: 45, display: '45 Years' },
            { key: 50, display: '50 Years' },
            { key: 55, display: '55 Years' },
            { key: 60, display: '60 Years' }
        ];
        if (currentAge >= 80) return [];
        if (currentAge >= 70) return _.filter(availAges, function(timeIn) { return timeIn.key <= 10; });
        if (currentAge >= 65) return _.filter(availAges, function (timeIn) { return timeIn.key <= 15; });
        if (currentAge >= 60) return _.filter(availAges, function (timeIn) { return timeIn.key <= 20; });
        if (currentAge >= 55) return _.filter(availAges, function (timeIn) { return timeIn.key <= 25; });
        if (currentAge >= 50) return _.filter(availAges, function (timeIn) { return timeIn.key <= 30; });
        if (currentAge >= 45) return _.filter(availAges, function (timeIn) { return timeIn.key <= 35; });
        if (currentAge >= 40) return _.filter(availAges, function (timeIn) { return timeIn.key <= 40; });
        if (currentAge >= 35) return _.filter(availAges, function (timeIn) { return timeIn.key <= 45; });
        if (currentAge >= 30) return _.filter(availAges, function (timeIn) { return timeIn.key <= 50; });
        if (currentAge >= 25) return _.filter(availAges, function (timeIn) { return timeIn.key <= 55; });
        if (currentAge >= 20) return _.filter(availAges, function (timeIn) { return timeIn.key <= 60; });
        return availAges;
    }
});