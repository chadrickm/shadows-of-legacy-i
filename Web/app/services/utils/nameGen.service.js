﻿'use strict';

game.factory('nameGenService',
    function() {

        var service = {
            getName: getName
        };

        return service;

        function getName() {
            var minLength = 3;
            var maxLength = 9;
            //var names = ['Aegon', 'Aenys', 'Maegor', 'Jaehaerys', 'Viserys', 'Daeron', 'Baelor', 'Aerys', 'Maekar', 'Rhaenyra', 'Daenerys', 'Rhaegar', 'Daemon', 'Maelys'];
            //var names = ['Tyrion', 'Jaime', 'Cersei', 'Jon', 'Jorah', 'Petyr', 'Tywin', 'Stannis', 'Margaery', 'Bran', 'Sansa', 'Arya', 'Theon', 'Joffrey', 'Samwell', 'Gendry', 'Sandor', 'Bronn', 'Rob', 'Catelyn', 'Jeor', 'Eddard', 'Robert'];
            var names = ['Tyrion', 'Jaime', 'Cersei', 'Tywin', 'Joffrey', 'Bran', 'Sansa', 'Arya', 'Rob', 'Catelyn', 'Eddard'];
            var nextLetterDictionary = makeDictionary(names);
            var pairs = makePairs(names);
            var firstLetters = getRandomPair(pairs);
            var name = createName(firstLetters, names, nextLetterDictionary, minLength, maxLength);
            return name.charAt(0).toUpperCase() + name.slice(1);
        }

        function createName(firstLetters, names, nextLetterDict, min, max) {
            var nameLength = getRandomInRange(min, max);
            var name = firstLetters;
            while (name.length < nameLength) {
                var lastLetter = name.substring(name.length - 1);
                var nextLetterEntry = _.find(nextLetterDict, function (entry) { return entry.letter == lastLetter; });
                if (nextLetterEntry == undefined) {
                    console.log('name so far:', name);
                } else {
                    var randomIndex = getRandomInRange(1, nextLetterEntry.letters.length) + -1;
                    var nextLetter = nextLetterEntry.letters.length == 1
                        ? nextLetterEntry.letters[0]
                        : nextLetterEntry.letters[randomIndex];
                    name += nextLetter;
                }
            }

            return name;
        }

        function makeDictionary(names) {
            var dict = [];
            _.each(names, function(name) {
                for (var i = 1; i < name.length; i++) {
                    var dictEntry = _.find(dict, function(entry) { return entry.letter == name[i]; });
                    if (dictEntry != undefined) {
                        if (dictEntry.letters == undefined) dictEntry.letters = [];
                        if (!_.contains(dictEntry.letters, name[i + 1])) {
                            if (name[i + 1] != undefined) {
                                dictEntry.letters.push(name[i + 1]);
                            }
                        }
                    } else {
                        if (name[i + 1] != undefined) {
                            dict.push({ letter: name[i], letters: [name[i + 1]] });
                        }
                    }
                }
            });

            return dict;
        }

        function getRandomPair(pairs) {
            var min = 1;
            var max = pairs.length;
            return pairs[getRandomInRange(min, max) - 1];
        }

        function makePairs(names) {
            var pairs = [];
            _.each(names, function(name) {
                var firstLetters = name.substring(0, 2);
                if (!_.contains(pairs, function() { return firstLetters; })) {
                    pairs.push(firstLetters);
                }
            });
            return pairs;
        }
        
        function getRandomInRange(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }
    }
);