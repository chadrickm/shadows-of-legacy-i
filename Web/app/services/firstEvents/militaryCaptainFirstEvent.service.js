﻿'use strict';

game.factory('militaryCaptainFirstEventService',
    ['settingTypesService',
        function (settingTypesService) {

            var service = {
                getEvent: getEvent
            };

            return service;

            function getEvent() {
                var randomSetting = settingTypesService.getRandomSetting();
                var event = {
                    description: 'A local ' + randomSetting.display
                };
            }
        }
    ]
);