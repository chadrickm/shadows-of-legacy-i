﻿'use strict';

game.factory('newBackgroundFormValidator', function() {
    var validator = {
        validate: validate,
        messages: [],
        isValid: isValid,
    };

    return validator;

    //public functions
    function validate(character, yearsInSetting) {
        validator.messages = [];
        //we don't start validating until things are defined
        if (character == undefined || yearsInSetting == undefined) return true;
        // do all our validations in private methods
        validateYearsInSetting(character, yearsInSetting);
    }

    function isValid() {
        return validator.messages.length == 0;
    }

    //private functions
    function validateYearsInSetting(character, yearsInSetting) {
        var maxYears = (60 - character.age());
        if (yearsInSetting < 1 || yearsInSetting > maxYears) {
            validator.messages.push('Years In Setting must be between 1-' + maxYears);
        }
    }
});