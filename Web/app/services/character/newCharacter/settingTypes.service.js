﻿'use strict'; 

game.factory('settingTypesService', function () {

    var service = {
        getBirthSettings: getBirthSettings,
        getAllSettings: getAllSettings,
        getStartingSettingByFrom: getStartingSettingByFrom,
        getStartingSettingByKey: getStartingSettingByKey,
        getRandomSetting: getRandomSetting,
        startingSettings: [
            {
                from: 'UrbanGuard',
                key: 'UrbanGuardCaptain',
                display: 'Urban Guard Captain',
                description: 'You are a captain within the local guard. Most of your subordinates are loyal to you alone.'
            },
            {
                from: 'Scholar',
                key: 'AdvisorMentor',
                display: 'Advisor and Mentor',
                description: 'You are a person that many come to when they want to know the truth about something. Your network of scholars can collect ' +
                    'and disseminate knowledge faster than most.'
            },
            {
                from: 'ReligiousDevotee',
                key: 'ReligiousLeader',
                display: 'Religious Leader',
                description: 'Devotees and average followers associate your name with the future of their religious system and/or their deity. You are a ' +
                    'voice that directs them.'
            },
            {
                from: 'CourtMemberServant',
                key: 'CourtAdvisor',
                display: 'Court Advisor',
                description: 'You have great influence at court. People listen to what you say and carry out your will whenever possible.'
            },
            {
                from: 'RuralPeasant',
                key: 'RuralPeasantLeader',
                display: 'Rural Peasant Leader',
                description: 'The people of your area know you or know of you as a local hero or villain. Your followers believe in you and your aims.'
            },
            {
                from: 'UrbanPeasant',
                key: 'UrbanPeasantLeader',
                display: 'Urban Peasant Leader',
                description: 'The people in your town/village/city fear you or look up to you and your followers.'
            },
            {
                from: 'UrbanMerchant',
                key: 'GuildLeader',
                display: 'Guild Leader',
                description: 'You have proven yourself as a shrewd business man/woman with quite a local network of people who help you succeed.'
            },
            {
                from: 'StreetThug',
                key: 'GangLeader',
                display: 'Gang Leader',
                description: 'You are the leader of street thugs. Some love you and others dispise you and those who follow you.'
            },
            {
                from: 'MercenaryCompany',
                key: 'MercCompanyLeader',
                display: 'Mercenary Company Leader',
                description: 'You are in a leadership role within a larger mercenary company. Most of your subordinates are loyal to you alone.'
            },
            {
                from: 'Soldier',
                key: 'MilitaryCaptain',
                display: 'Military Captain',
                description: 'You are in a leadership role within the local military. Most of your subordinates are loyal to you alone.'
            },
            {
                from: 'MerchantSailor',
                key: 'MerchantShipCaptain',
                display: 'Merchant Ship Captain',
                description: 'You have sailed on the seas and those who have sailed with you trust you and your judgement to find a great and make a deal accross the seas.'
            },
            {
                from: 'NavySailor',
                key: 'NavySailorCaptain',
                display: 'Navy Sailor Captain',
                description: 'You have seen action on the seas. Your followers will go wherever you tell them to sail.'
            },
            {
                from: 'Slave',
                key: 'SlaversMaster',
                display: 'Master of Slavers',
                description: 'You have learned how to make your own will happen within the confines of your servitude. Your followers, some slave and some free, help you make your will happen.'
            },
            {
                from: 'Wanderer',
                key: "WanderersLeader",
                display: "Wanderer's Leader",
                description: 'The wanderers have come to you over time. They follow you now and carry out your will.'
            },
            {
                from: 'Prisoner',
                key: 'PrisonerByChoice',
                display: 'Prisoner by Choice',
                description: 'Those that hold you as a prisoner and fellow prisoners have come to take your lead. They find ways to make your will done.'
            }]
    };
    
    return service;

    function getRandomSetting() {
        var settings = getAllSettings();
        var randomSetting = settings[Math.floor((Math.random() * settings.length) + 1)];
        return randomSetting;
    }

    function getBirthSettings() {
        var settings = [
            {
                key: 'RuralPeasants',
                display: 'Rural Peasant Family',
                orderBy: 1,
                description: 'Like most people in Keveil, you were born to a peasant family. Probably farmers or crafters of some type.',
                nextSettings: ['ReligiousDevotee', 'RuralPeasant', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'UrbanPeasant',
                display: 'Urban Peasant Family',
                orderBy: 2,
                description: 'Like many people in Keveil, your family spent most of their time in a village or town. They were probably servants or laborers of some type.',
                nextSettings: ['ReligiousDevotee', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'UrbanMerchants',
                display: 'Urban Merchant Family',
                orderBy: 4,
                description: 'Your family opperated a business in a village or town.',
                nextSettings: ['CourtMemberServant', 'Scholar', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'UrbanStreet',
                display: 'Street Kid',
                orderBy: 3,
                description: "You don't know where your parents were and they didn't know or care where you were. The other kids on the streets were your real family.",
                nextSettings: ['ReligiousDevotee', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'MerchantSailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'SlaveToNobility',
                display: 'Slave Family Serving Nobility in Tikor',
                orderBy: 6,
                description: 'Your mother, and consequently you, were the property of a Tikori noble house to the north of Keveil.',
                nextSettings: ['ReligiousDevotee', 'RuralPeasant', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'SlaveToMility',
                display: 'Slave Family Serving a Mercenary Company in the Free Cities',
                orderBy: 5,
                description: 'A mercenary company owned your mother, and perhaps your father, in the Free Cities south of Keveil. Children of military slaves were often ' +
                    'killed or sold somewhere along the campaign. Most of your childhood would have been spent in your next setting.',
                nextSettings: ['ReligiousDevotee', 'RuralPeasant', 'UrbanPeasant', 'Slave', 'Wanderer']
            },
            {
                key: 'IllegitimateNoble',
                display: 'Illegitimate Born in a Noble Family in Tikor or Keveil',
                orderBy: 7,
                description: "Your mother was not the Lord's wife. You were probably shunned by most. Perhaps the Lord cared for your mother and you, perhaps not.",
                nextSettings: ['ReligiousDevotee', 'Scholar', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'MinorHouse',
                display: 'Born Noble in a Minor House of Keveil',
                orderBy: 8,
                description: 'You were born to privilege. With that privilege came responsability. You may have taken that responsability seriously, or you may not have.',
                nextSettings: ['CourtMemberServant', 'ReligiousDevotee', 'Scholar', 'UrbanGuard', 'StreetThug', 'MercenaryCompany', 'Soldier', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            }
        ];

        return _.sortBy(settings, function (setting) { return setting.orderBy });
    }

    function getAllSettings() {
        var settings = [
            {
                key: 'UrbanGuard',
                display: 'Urban Guard',
                description:
                    'You were a keeper of peace and justice in the city. Your definition of peace and justice might not have always been everyone elses ' +
                    'definition.',
                nextSettings: ['CourtMemberServant', 'ReligiousDevotee', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'MercenaryCompany', 'MerchantSailor', 'NavySailor', 'Wanderer', 'Prisoner']
            },
            {
                key: 'Scholar',
                display: 'Scholar',
                description:
                    'You have studied many subjects and learned the ways of things in deeper ways than most. The more you learned the more you found there ' +
                    'was to learn.',
                nextSettings: ['CourtMemberServant', 'ReligiousDevotee', 'UrbanMerchant', 'Wanderer', 'Prisoner']
            },
            {
                key: 'ReligiousDevotee',
                display: 'Religious Devotee',
                description:
                    'You were or are devoted to a religious system and/or a diety worshiped by many in the lands. Your devotion led you to do more than the ' +
                    'average follower.',
                nextSettings: ['CourtMemberServant', 'Scholar', 'UrbanGuard', 'RuralPeasant', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'NavySailor', 'Wanderer', 'Prisoner']
            },
            {
                key: 'CourtMemberServant',
                display: 'Court Servant',
                description:
                    'You worked within some kind of court as a servant to people in high places. You heard things and saw things that ' +
                    'people only dream of.',
                nextSettings: ['ReligiousDevotee', 'Scholar', 'UrbanGuard', 'UrbanMerchant', 'MerchantSailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'RuralPeasant',
                display: 'Rural Peasant',
                description:
                    'You worked as a hired hand or was given the grant of land for your daily bread. A portion of your time or your produce was given to the ' +
                    'local land owner.',
                nextSettings: ['ReligiousDevotee', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'UrbanPeasant',
                display: 'Urban Peasant',
                description:
                    'You worked as a hired hand or were assined cottage and labored for your daily bread. A portion of your time or your produce was given to the ' +
                    'local lord.',
                nextSettings: ['ReligiousDevotee', 'Scholar', 'RuralPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'UrbanMerchant',
                display: 'Urban Merchant',
                description:
                    'You worked for a merchant or somehow you escaped the life of a peasant and opened your own shop or spent your time selling from town to town ' +
                    'as a traveling merchant.',
                nextSettings: ['CourtMemberServant', 'ReligiousDevotee', 'Scholar', 'RuralPeasant', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'StreetThug',
                display: 'Street Thug',
                description:
                    'You made your way on the streets with others of like mind. It was all about making the most of the opportunities as they presented themselves. ' +
                    'Sometimes life was harsh and other times it was full of abundance.',
                nextSettings: ['ReligiousDevotee', 'UrbanGuard', 'UrbanMerchant', 'MercenaryCompany', 'MerchantSailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'MercenaryCompany',
                display: 'Mercenary Soldier',
                description:
                    'You joined one of the companies and learned to kill for a living. It was hard going, but your belly was full and your friends had your back.',
                nextSettings: ['CourtMemberServant', 'ReligiousDevotee', 'UrbanGuard', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'Soldier',
                display: 'Millitary Soldier',
                description:
                    'You proved yourself useful to the lord in battle and he kept you on as part of a standing army or as a trainer of others.',
                nextSettings: ['CourtMemberServant', 'ReligiousDevotee', 'Scholar', 'UrbanGuard', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'MerchantSailor',
                display: 'Merchant Sailor',
                description:
                    'You found your way onto a ship headed for distant shores.',
                nextSettings: ['CourtMemberServant', 'Scholar', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'NavySailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'NavySailor',
                display: 'Navy Sailor',
                description:
                    'You worked aboard a navy ship carrying soldiers to their battles and fighting your own battles with other ships at sea.',
                nextSettings: ['CourtMemberServant', 'Scholar', 'UrbanGuard', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'Slave', 'Wanderer', 'Prisoner']
            },
            {
                key: 'Slave',
                display: 'Slave',
                description:
                    'You were put in chains, either literal or emotional. Your life was not your own to command.',
                nextSettings: ['ReligiousDevotee', 'Scholar', 'RuralPeasant', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Wanderer', 'Prisoner']
            },
            {
                key: 'Wanderer',
                display: 'Wanderer',
                description:
                    'You wandered the world looking for adventure or fleeing your past.',
                nextSettings: ['ReligiousDevotee', 'Scholar', 'RuralPeasant', 'UrbanPeasant', 'UrbanMerchant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Prisoner']
            },
            {
                key: 'Prisoner',
                display: 'Prisoner',
                description:
                    "You were inprisoned for something you did or didn't do. You were probably forced to do hard labor or spent your time wasting away in a cell.",
                nextSettings: ['ReligiousDevotee', 'RuralPeasant', 'UrbanPeasant', 'StreetThug', 'MercenaryCompany', 'Soldier', 'MerchantSailor', 'NavySailor', 'Slave', 'Wanderer']
            }
        ];

        return _.sortBy(settings, function (setting) { return setting.display } );
    }
    
    function getStartingSettingByKey(settingKey) {
        return _.find(service.startingSettings, function (setting) {
            return setting.key == settingKey;
        });
    }

    function getStartingSettingByFrom(settingFrom) {
        return _.find(service.startingSettings, function (setting) {
            return setting.from == settingFrom;
        });
    }
});