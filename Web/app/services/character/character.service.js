﻿'use strict';

game.factory('characterService',
    ['breeze', 'characterDataContext', 'settingTypesService', 'nameGenService',
        function (breeze, context, settingTypesService, nameGenService) {

            var service = {

                untilAge: 15,

                getCharacter: getCharacter,
                getCharacters: getCharacters,
                getStartedCharacters: getStartedCharacters,
                createCharacter: createCharacter,
                saveCharacter: saveCharacter,
                getNames: getNames
                
            };

            return service;
            
            //public functions
            function getNames() {
                var names = [];
                while(names.length < 1) {
                    var name = nameGenService.getName();
                    names.push(name);
                }
                return names;
            }

            function saveCharacter(character) {
                return context.saveEntity(character);
            }

            function createCharacter() {
                return context.createCharacter();
            }

            function getStartedCharacters(forceRefresh) {

                var query = breeze.EntityQuery
                    .from("Characters")
                    .expand("BackgroundSettings")
                    .where("startingSetting", "!=", null);

                return context.getCharacters(forceRefresh, query)
                    .then(function (data) { return data; })
                    .fail(failed);
            }

            function getCharacters(forceRefresh) {

                var query = breeze.EntityQuery
                    .from("Characters")
                    .expand("BackgroundSettings");

                return context.getCharacters(forceRefresh, query)
                    .then(function (data) { return data; })
                    .fail(failed);
            }

            function getCharacter(forceRefresh) {

                var query = breeze.EntityQuery
                    .from("Characters")
                    .where("userId", "==", userId)
                    .expand("BackgroundSettings");

                return context.getCharacters(forceRefresh, query).then(getCharactersSucceeded).fail(failed);
            }
            
            //private functions
            function getCharactersSucceeded(data) {
                var character = data[0];
                var adultSettings = settingTypesService.getAllSettings();
                _.each(character.backgroundSettings, function (characterSetting) {
                    var typeSetting = _.find(adultSettings, function (clientSetting) {
                        return clientSetting.key == characterSetting.settingKey;
                    });

                    service.untilAge += characterSetting.yearsInSetting;

                    characterSetting.untilAge = service.untilAge;
                    characterSetting.key = typeSetting.key;
                    characterSetting.display = typeSetting.display;
                    characterSetting.description = typeSetting.description;
                });

                return character;
            }

            function failed(error) {
                console.log(error.message);
            }
        }
    ]
);