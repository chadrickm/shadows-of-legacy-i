﻿/* model: extend server-supplied metadata with client-side entity model members */
game.factory('characterModel', ['settingTypesService', '$route',
    function (typesService, $route) {

    var datacontext;
    
    extendCharacter();

    var characterModel = {
        initialize: initialize
    };
    
    return characterModel;
  
    function initialize(context) {
        datacontext = context;
        var store = datacontext.metadataStore;
        store.registerEntityTypeCtor("Character", Character, null);
    }
    
    function Character() { }
    
    function extendCharacter() {

        Character.prototype.age = function () {
            if (this.birthSetting == undefined) return 0;
            var age = 15;
            _.each(this.backgroundSettings, function (setting) {
                age += setting.yearsInSetting;
            });
            return age;
        };

        Character.prototype.addBackgroundSetting = function (backgroundSetting, yearsInSetting, untilAge) {
            var newUntilAge = parseInt(untilAge) + parseInt(yearsInSetting);
            var serverBackgroundSetting = datacontext.createBackgroundSetting();
            serverBackgroundSetting.characterId = this.characterId;
            serverBackgroundSetting.orderBy = this.backgroundSettings == undefined ? 0 : this.backgroundSettings.length;
            serverBackgroundSetting.settingKey = backgroundSetting.key;
            serverBackgroundSetting.display = backgroundSetting.display;
            serverBackgroundSetting.description = backgroundSetting.description;
            serverBackgroundSetting.nextSettings = backgroundSetting.nextSettings;
            serverBackgroundSetting.yearsInSetting = yearsInSetting;
            serverBackgroundSetting.untilAge = newUntilAge;
            this.backgroundSettings.push(serverBackgroundSetting);
            datacontext.saveEntity(this);
            return newUntilAge;
        };

        Character.prototype.setLastSettingAsStartingSetting = function(lastSetting) {
            var startingSetting = typesService.getStartingSettingByFrom(lastSetting.key);
            this.startingSetting = startingSetting.key;
            this.isBackgroundFinished = true;
            this.startingAge = this.age();
            datacontext.saveEntity(this);
        };

        Character.prototype.deleteBackgroundSetting = function(backgroundSetting) {
            if (this.startingSetting != undefined) {
                this.startingSetting = undefined;
                this.isBackgroundFinished = false;
                this.startingAge = 0;
            }
            _.each(this.backgroundSettings, function (setting) {
                if (backgroundSetting.backgroundSettingId == setting.backgroundSettingId) {
                    setting.entityAspect.setDeleted();
                }
            });
            return datacontext.saveEntity(this);
        };
    }
}]);