﻿'use strict';

game.controller('allPlayersController',
    ['$scope', '$location', 'breeze', '_', 'characterService',
        function($scope, $location, breeze, _, characterService) {

            $scope.refreshView = refreshView;
            $scope.characters = undefined;
            $scope.getCharacters = getCharacters;
            $scope.getCharacters(true);
            $scope.oldestAge = 0;
            $scope.width = 700;

            function refreshView() {
                $scope.$apply();
            }

            function getCharacters(forceRefresh) {
                characterService.getStartedCharacters(forceRefresh)
                    .then(function (data) {
                        $scope.characters = data;
                        $scope.oldestAge = getOldestAge();
                        _.each($scope.characters, function(character) {
                            character.beforeBirthWidth = convertToNgWidthValue($scope.oldestAge - character.startingAge);
                            character.birthSettingWidth = convertToNgWidthValue(15);
                            _.each(character.backgroundSettings, function(setting) {
                                setting.settingWidth = convertToNgWidthValue(setting.yearsInSetting);
                            });
                        });
                    })
                    .fail(function (error) { console.log(error.message); })
                    .fin(refreshView);
            }
            
            function getOldestAge() {
                var oldestCharacter = _.max($scope.characters, function(character) { return character.startingAge; });
                return oldestCharacter.startingAge;
            }
            
            function convertToNgWidthValue(years) {
                if (years == 0) return { width: 0 + 'px' };
                var percent = years / $scope.oldestAge;
                return {
                    width: parseInt($scope.width * percent) + 'px'
                };
            }

        }
    ]
);