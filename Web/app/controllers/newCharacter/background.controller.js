﻿'use strict';

game.controller('BackgroundCtrl',
    ['$scope', '$location', 'breeze', '_', 'logger', 'characterService', 'settingTypesService', 'newBackgroundFormValidator',
    function ($scope, $location, breeze, _, logger, characterService, settingTypesService, validator) {

        $scope.refreshView = refreshView;
        $scope.character = getCharacter(true);
        $scope.validator = validator;
        $scope.endEdit = endEdit;
        $scope.addBackgroundSetting = addBackgroundSetting;
        $scope.refreshView = refreshView;
        $scope.selectedBirthSetting = selectedBirthSetting;
        $scope.birthSettings = settingTypesService.getBirthSettings();
        var adultSettings = settingTypesService.getAllSettings();
        $scope.lastSetting = getLastSetting;
        $scope.nextSettings = getNextSettings;
        $scope.selectedNextSetting = undefined;
        $scope.yearsInSetting = 5;
        $scope.untilAge = 0;
        $scope.startingSetting = startingSetting;
        $scope.deleteBackgroundSetting = deleteBackgroundSetting;
        $scope.setLastSettingAsStartingSetting = setLastSettingAsStartingSetting;


        function refreshView() {
            $scope.$apply();
        }

        function deleteBackgroundSetting(settingToDelete) {
            confirm('This will delete all settings after this one as well. Are you sure?');
            var settingsToDelete = _.filter($scope.character.backgroundSettings, function(charSetting) {
                return charSetting.orderBy >= settingToDelete.orderBy;
            });
            _.each(settingsToDelete, function(setting) {
                $scope.character.deleteBackgroundSetting(setting);
            });
        }

        function startingSetting() {
            if ($scope.character == undefined || $scope.character.startingSetting == undefined) return undefined;
            return settingTypesService.getStartingSettingByKey($scope.character.startingSetting);
        }

        function setLastSettingAsStartingSetting() {
            $scope.character.setLastSettingAsStartingSetting($scope.lastSetting());
        }

        function addBackgroundSetting() {
            var character = $scope.character;
            $scope.untilAge = character.addBackgroundSetting($scope.selectedNextSetting, $scope.yearsInSetting, $scope.untilAge);
            $scope.selectedNextSetting = undefined;
        }

        function getLastSetting() {
            if ($scope.character == undefined) return undefined;
            
            if ($scope.character.backgroundSettings.length > 0) {
                return _.find(adultSettings, function (setting) {
                    return setting.key == _.last($scope.character.backgroundSettings).settingKey;
                });
            }
            // this will change when we start saving background settings on the character but for now just use the birthsetting to make it work
            return _.find($scope.birthSettings, function (setting) {
                return setting.key == $scope.character.birthSetting;
            });
        }
            
        function getNextSettings() {
            if ($scope.lastSetting() == undefined) return undefined;
            var nextSettings = [];
            _.each($scope.lastSetting().nextSettings, function(nextSettingKey) {
                var setting = _.find(adultSettings, function(settingObj) {
                    return settingObj.key == nextSettingKey;
                });
                nextSettings.push(setting);
            });

            return _.sortBy(nextSettings, function (setting) { return setting.display; });
        }

        function selectedBirthSetting() {
            return _.find($scope.birthSettings, function(setting) {
                if ($scope.character == undefined) return undefined;
                return setting.key == $scope.character.birthSetting;
            });
        }

        function getCharacter(forceRefresh) {
            characterService.getCharacter(forceRefresh)
                .then(function(character) {
                    if (character == undefined) {
                        character = characterService.createCharacter();
                    }

                    $scope.character = character;
                    $scope.untilAge = characterService.untilAge;
                })
                .fin(refreshView);
        }

        function endEdit(entity) {
            characterService.saveCharacter(entity).then(editSucceeded).fail(editFailed).fin($scope.refreshView);

            function editSucceeded() {
                $location.path('/character/background');
            }

            function editFailed() {

            }
        }

    }]);