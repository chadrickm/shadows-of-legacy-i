﻿'use strict';

game.controller('CharacterCtrl',
    ['$scope', '$location', 'breeze', 'logger', 'characterService',
        function($scope, $location, breeze, logger, characterService) {

            $scope.refreshView = refreshView;
            $scope.character = undefined;
            getCharacter(true);
            $scope.endEdit = endEdit;
            //$scope.names = _.sortBy(_.sortBy(characterService.getNames(), function (name) { return name; }), function (name) { return name.length; });

            // load Characters immediately (from cache if possible)
            $scope.showNewCharacterSubmit = showNewCharacterSubmit;

            function showNewCharacterSubmit() {
                if ($scope.character == undefined) return false;
                return $scope.character.firstName != '';
            }

            function getCharacter(forceRefresh) {
                characterService.getCharacter(forceRefresh)
                    .then(function(character) {
                        if (character == undefined) {
                            character = characterService.createCharacter();
                        }

                        $scope.character = character;
                    })
                    .fail(function(error) {
                        console.log('error', error.message);
                    })
                    .fin(refreshView);
            }

            function refreshView() {
                $scope.$apply();
            }

            function endEdit(entity) {
                characterService.saveCharacter(entity).then(addSucceeded).fail(addFailed).fin(refreshView);

                function addSucceeded() {
                    if (!$scope.character.isBackgroundFinished) {
                        $location.path('/character/background');
                    }
                }

                function addFailed(error) {
                    console.log("Save of new character failed");
                    console.log(error);
                }
            }

        }
    ]
);