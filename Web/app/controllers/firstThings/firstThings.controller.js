﻿'use strict';

game.controller('firstThingsCtrl', 
    ['$scope', 'firstEventService', 'characterService',
        function($scope, firstEventService, characterService) {

            $scope.refreshView = refreshView;

            $scope.character = undefined;
            $scope.event = undefined;
            
            getCharacter(true);
            getEvent();
            
            function refreshView() {
                $scope.$apply();
            }

            function getCharacter(forceRefresh) {
                characterService.getCharacter(forceRefresh)
                    .then(function(character) {
                        $scope.character = character;
                    })
                    .fin(refreshView);
            }

            function getEvent() {
                firstEventService.getFirstEvent(character.startingSetting);
            }

        }
    ]
);