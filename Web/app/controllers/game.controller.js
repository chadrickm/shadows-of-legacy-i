﻿'use strict';

game.controller('GameCtrl', ['$scope', '$location', 'breeze', 'logger', 'characterDataContext', 'DomainDataContext',
    function($scope, $location, breeze, logger, characterDataContext, domainDataContext) {

        logger.log('creating GameCtrl');

        $scope.characters = [];
        $scope.domain = {};
        $scope.hasCharacter = false;
        
        $scope.error = '';

        $scope.refreshView = refreshView;
        $scope.getDomainsSucceeded = getDomainsSucceeded;
        $scope.getCharactersSucceeded = getCharactersSucceeded;
        $scope.getCharacters = getCharacters;
        $scope.getDomain = getDomain;

        $scope.loadingMessage = 'Starting Game Engine';

        $scope.getCharacters();
        $scope.getDomain();

        function getCharacters(forceRefresh) {
            $scope.loadingMessage = 'Checking for Character(s)';

            var query = breeze.EntityQuery
                .from("Characters")
                .expand("BackgroundSettings")
                .where("userId", "==", userId);

            characterDataContext
                .getCharacters(forceRefresh, query)
                .then(getCharactersSucceeded)
                .fail(failed)
                .fin(refreshView);
        }

        function getDomain(forceRefresh) {
            domainDataContext
                .getDomains(forceRefresh)
                .then(getDomainsSucceeded)
                .fail(failed)
                .fin(refreshView);
        }

        function refreshView() {
            $scope.$apply();
        }

        function getCharactersSucceeded(data) {
            $scope.characters = data;
            $scope.hasCharacter = $scope.characters.length > 0;
            if (!$scope.hasCharacter) {
                //console.log('no character yet');
                $location.path('/character/new');
            } else { //if (!$scope.characters[0].isBackgroundFinished) {
                $location.path('/character/background');
            }
        }

        function getDomainsSucceeded(data) {
            $scope.domain = data.arrayFirst;
        }

        function failed(error) {
            $scope.error = error.message;
        }

    }]);