﻿'use strict';
/* Defines the controller
 * Constructor function relies on Ng injector to provide:
 *     $scope - context variable for the view to which the view binds
 *     breeze - breeze is a "module" known to the injectory thanks to main.js
 *     datacontext - injected data and model access component (todo.datacontext.js)
 *     logger - records notable events during the session (about.logger.js)
 */

game.controller('DomainCtrl',
    ['$scope', 'breeze', 'DomainDataContext', 'logger',
        function($scope, breeze, datacontext, logger) {

            logger.log("creating DomainCtrl");
            var removeList = breeze.core.arrayRemoveItem;

            $scope.domains = [];
            $scope.error = "";
            $scope.getDomains = getDomains;
            $scope.refresh = refresh;
            $scope.endEdit = endEdit;
            $scope.addDomain = addDomain;
            $scope.deleteDomain = deleteDomain;
            $scope.clearErrorMessage = clearErrorMessage;

            // load TodoLists immediately (from cache if possible)
            $scope.getDomains();

            //#region private functions

            function getDomains(forceRefresh) {
                datacontext.getDomains(forceRefresh)
                    .then(getSucceeded)
                    .fail(failed)
                    .fin(refreshView);
            }

            function refresh() { getDomains(true); }

            function getSucceeded(data) {
                $scope.domains = data;
            }

            function failed(error) {
                $scope.error = error.message;
            }

            function refreshView() {
                console.log("refreshView");
                $scope.$apply();
            }

            function endEdit(entity) {
                datacontext.saveEntity(entity).fin(refreshView);
            }

            function addDomain() {
                var domain = datacontext.createDomain();
                //todoList.isEditingListTitle = true;
                datacontext.saveDomain(domain)
                    .then(addSucceeded)
                    .fail(addFailed)
                    .fin(refreshView);

                function addSucceeded() {
                    //todo: not sure what to do here
                    //showAddedTodoList(todoList);
                }

                function addFailed(error) {
                    failed({ message: "Save of new domain failed" });
                }
            }

            function deleteDomain(domain) {
                removeDomain($scope.domain, domain);
                console.log("deleteDomain");
                datacontext.deleteDomain(domain)
                    .fail(deleteFailed)
                    .fin(refreshView);

                function deleteFailed() {
                    //showAddedTodoList(todoList); // re-show the restored list
                }
            }

            function clearErrorMessage(obj) {
                if (obj && obj.errorMessage) {
                    obj.errorMessage = null;
                }
            }

            //#endregion
        }]);